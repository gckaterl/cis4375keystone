json.array!(@bid_sheets) do |bid_sheet|
  json.extract! bid_sheet, :id, :employee_id, :customer_id, :jobNumber, :generator_id, :transferSwitch_id, :concreteSlab_id, :laborCost, :tax, :createDate
  json.url bid_sheet_url(bid_sheet, format: :json)
end
