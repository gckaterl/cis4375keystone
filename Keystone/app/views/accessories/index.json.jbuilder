json.array!(@accessories) do |accessory|
  json.extract! accessory, :id, :productDescription, :modelNumber, :dealer, :msrp
  json.url accessory_url(accessory, format: :json)
end
