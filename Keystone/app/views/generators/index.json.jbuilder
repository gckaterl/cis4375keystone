json.array!(@generators) do |generator|
  json.extract! generator, :id, :productDescription, :modelNumber, :phVolt, :dealer, :msrp, :map, :voltage, :mountedBreaker, :alternator
  json.url generator_url(generator, format: :json)
end
