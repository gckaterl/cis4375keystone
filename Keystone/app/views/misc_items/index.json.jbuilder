json.array!(@misc_items) do |misc_item|
  json.extract! misc_item, :id, :name, :price
  json.url misc_item_url(misc_item, format: :json)
end
