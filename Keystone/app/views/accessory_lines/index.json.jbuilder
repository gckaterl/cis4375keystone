json.array!(@accessory_lines) do |accessory_line|
  json.extract! accessory_line, :id, :bidSheet_id, :accessory_id
  json.url accessory_line_url(accessory_line, format: :json)
end
