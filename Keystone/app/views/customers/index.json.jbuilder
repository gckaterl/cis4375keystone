json.array!(@customers) do |customer|
  json.extract! customer, :id, :firstName, :lastName, :streetAddress, :city, :state_id, :zipCode, :phone, :email
  json.url customer_url(customer, format: :json)
end
