json.array!(@misc_item_lines) do |misc_item_line|
  json.extract! misc_item_line, :id, :bidSheet_id, :accessory_id, :quantity
  json.url misc_item_line_url(misc_item_line, format: :json)
end
