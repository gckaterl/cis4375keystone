class MiscItemsController < ApplicationController
  before_action :set_misc_item, only: [:show, :edit, :update, :destroy]

  # GET /misc_items
  # GET /misc_items.json
  def index
    @misc_items = MiscItem.all
  end

  # GET /misc_items/1
  # GET /misc_items/1.json
  def show
  end

  # GET /misc_items/new
  def new
    @misc_item = MiscItem.new
  end

  # GET /misc_items/1/edit
  def edit
  end

  # POST /misc_items
  # POST /misc_items.json
  def create
    @misc_item = MiscItem.new(misc_item_params)

    respond_to do |format|
      if @misc_item.save
        format.html { redirect_to @misc_item, notice: 'Misc item was successfully created.' }
        format.json { render :show, status: :created, location: @misc_item }
      else
        format.html { render :new }
        format.json { render json: @misc_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /misc_items/1
  # PATCH/PUT /misc_items/1.json
  def update
    respond_to do |format|
      if @misc_item.update(misc_item_params)
        format.html { redirect_to @misc_item, notice: 'Misc item was successfully updated.' }
        format.json { render :show, status: :ok, location: @misc_item }
      else
        format.html { render :edit }
        format.json { render json: @misc_item.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /misc_items/1
  # DELETE /misc_items/1.json
  def destroy
    @misc_item.destroy
    respond_to do |format|
      format.html { redirect_to misc_items_url, notice: 'Misc item was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_misc_item
      @misc_item = MiscItem.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def misc_item_params
      params.require(:misc_item).permit(:name, :price)
    end
end
