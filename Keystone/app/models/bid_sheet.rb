class BidSheet < ActiveRecord::Base
  belongs_to :employee
  belongs_to :customer
  belongs_to :generator
  belongs_to :transfer_switch
  belongs_to :concrete_slab

  has_many :accessory_lines
  has_many :accessories, :through => :accessory_lines

  has_many :misc_item_lines
  has_many :misc_items, :through => :misc_item_lines
end
