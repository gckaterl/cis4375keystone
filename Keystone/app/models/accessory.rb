class Accessory < ActiveRecord::Base
  has_many :bid_sheets
  has_many :accessory_lines, :through => :bid_sheets
end
