class MiscItem < ActiveRecord::Base
  has_many :bid_sheets
  has_many :misc_item_lines, :through => :bid_sheets
end
