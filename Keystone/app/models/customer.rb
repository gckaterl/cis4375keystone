class Customer < ActiveRecord::Base
  belongs_to :state
  has_many :bid_sheets
end
