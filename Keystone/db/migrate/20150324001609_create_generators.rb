class CreateGenerators < ActiveRecord::Migration
  def change
    create_table :generators do |t|
      t.string :productDescription
      t.string :modelNumber
      t.string :phVolt
      t.decimal :dealer, precision => 2
      t.decimal :msrp, precision => 2
      t.decimal :map, precision => 2
      t.string :voltage
      t.string :mountedBreaker
      t.string :alternator

      t.timestamps
    end
  end
end
