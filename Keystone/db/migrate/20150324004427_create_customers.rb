class CreateCustomers < ActiveRecord::Migration
  def change
    create_table :customers do |t|
      t.string :firstName
      t.string :lastName
      t.string :streetAddress
      t.string :streetAddress2
      t.string :city
      t.integer :state_id
      t.integer :zipCode
      t.integer :phone
      t.string :email

      t.timestamps
    end
  end
end
