class CreateMiscItemLines < ActiveRecord::Migration
  def change
    create_table :misc_item_lines do |t|
      t.integer :bidSheet_id
      t.integer :accessory_id
      t.integer :quantity

      t.timestamps
    end
  end
end
