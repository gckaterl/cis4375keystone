class CreateConcreteSlabs < ActiveRecord::Migration
  def change
    create_table :concrete_slabs do |t|
      t.string :name
      t.decimal :price, :precision => 2

      t.timestamps
    end
  end
end
