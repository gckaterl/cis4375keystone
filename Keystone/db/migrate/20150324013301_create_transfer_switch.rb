class CreateTransferSwitch < ActiveRecord::Migration
  def change
    create_table :transfer_switches do |t|
      t.string :productDescription
      t.string :modelNumber
      t.decimal :dealer, :precision => 2
      t.decimal :msrp, :precision => 2
      t.decimal :map, :precision => 2

      t.timestamps
    end
  end
end
