class CreateAccessories < ActiveRecord::Migration
  def change
    create_table :accessories do |t|
      t.string :productDescription
      t.string :modelNumber
      t.decimal :dealer, :precision => 2
      t.decimal :msrp, :precision => 2

      t.timestamps
    end
  end
end
