class CreateBidSheets < ActiveRecord::Migration
  def change
    create_table :bid_sheets do |t|
      t.integer :employee_id
      t.integer :customer_id
      t.integer :jobNumber
      t.integer :generator_id
      t.integer :transferSwitch_id
      t.integer :concreteSlab_id
      t.decimal :laborCost, :precision => 2
      t.decimal :tax, :precision => 2
      t.date :createDate

      t.timestamps
    end
  end
end
