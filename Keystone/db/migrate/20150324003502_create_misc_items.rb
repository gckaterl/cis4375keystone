class CreateMiscItems < ActiveRecord::Migration
  def change
    create_table :misc_items do |t|
      t.string :name
      t.decimal :price, :precision => 2

      t.timestamps
    end
  end
end
