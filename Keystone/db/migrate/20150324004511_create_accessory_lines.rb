class CreateAccessoryLines < ActiveRecord::Migration
  def change
    create_table :accessory_lines do |t|
      t.integer :bidSheet_id
      t.integer :accessory_id

      t.timestamps
    end
  end
end
