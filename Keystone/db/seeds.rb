# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)


# place scripts in order of priority (1's first)
# remember to place files in the "Keystone/db folder :)"


State.delete_all
open("State.txt") do |states|
  states.read.each_line do |data|
    name, abbreviation = data.chomp.split(",")
    State.create!(:name => name, :abbreviation => abbreviation)
  end
end

Customer.delete_all
open("Customer.txt") do |customers|
  customers.read.each_line do |data|
    firstName, lastName, streetAddress, streetAddress2, city, state_id, zipCode, phone, email = data.chomp.split(",")
    Customer.create!(:firstName => firstName, :lastName => lastName, :streetAddress => streetAddress,
                     :streetAddress2 => streetAddress2, :city => city, :state_id => state_id,
                     :zipCode => zipCode, :phone => phone, :email => email)
  end
end

Accessory.delete_all
open("Accessory.txt") do |accessories|
  accessories.read.each_line do |data|
    productDescription, modelNumber, dealer, msrp = data.chomp.split(",")
    Accessory.create!(:productDescription => productDescription, :modelNumber => modelNumber, :dealer => dealer)
  end
end

BidSheet.delete_all
open("BidSheet.txt") do |bidSheets|
  bidSheets.read.each_line do |data|
    employee_id, customer_id, jobNumber, generator_id, transferSwitch_id, concreteSlab_id, laborCost, tax = data.chomp.split(",")
    BidSheet.create!(:employee_id => employee_id, :customer_id => customer_id, :jobNumber => jobNumber, :generator_id => generator_id,
                     :transferSwitch_id => transferSwitch_id, :concreteSlab_id => concreteSlab_id, :laborCost => laborCost, :tax => tax)
  end
end

AccessoryLine.delete_all
open("AccessoryLine.txt") do |accessoryLines|
  accessoryLines.read.each_line do |data|
    bidSheet_id, accessory_id = data.chomp.split(",")
    AccessoryLine.create!(:bidSheet_id => bidSheet_id, :accessory_id => accessory_id)
  end
end