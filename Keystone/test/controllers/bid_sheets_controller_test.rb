require 'test_helper'

class BidSheetsControllerTest < ActionController::TestCase
  setup do
    @bid_sheet = bid_sheets(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:bid_sheets)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create bid_sheet" do
    assert_difference('BidSheet.count') do
      post :create, bid_sheet: { concreteSlab_id: @bid_sheet.concreteSlab_id, createDate: @bid_sheet.createDate, customer_id: @bid_sheet.customer_id, employee_id: @bid_sheet.employee_id, generator_id: @bid_sheet.generator_id, jobNumber: @bid_sheet.jobNumber, laborCost: @bid_sheet.laborCost, tax: @bid_sheet.tax, transferSwitch_id: @bid_sheet.transferSwitch_id }
    end

    assert_redirected_to bid_sheet_path(assigns(:bid_sheet))
  end

  test "should show bid_sheet" do
    get :show, id: @bid_sheet
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @bid_sheet
    assert_response :success
  end

  test "should update bid_sheet" do
    patch :update, id: @bid_sheet, bid_sheet: { concreteSlab_id: @bid_sheet.concreteSlab_id, createDate: @bid_sheet.createDate, customer_id: @bid_sheet.customer_id, employee_id: @bid_sheet.employee_id, generator_id: @bid_sheet.generator_id, jobNumber: @bid_sheet.jobNumber, laborCost: @bid_sheet.laborCost, tax: @bid_sheet.tax, transferSwitch_id: @bid_sheet.transferSwitch_id }
    assert_redirected_to bid_sheet_path(assigns(:bid_sheet))
  end

  test "should destroy bid_sheet" do
    assert_difference('BidSheet.count', -1) do
      delete :destroy, id: @bid_sheet
    end

    assert_redirected_to bid_sheets_path
  end
end
