require 'test_helper'

class MiscItemsControllerTest < ActionController::TestCase
  setup do
    @misc_item = misc_items(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:misc_items)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create misc_item" do
    assert_difference('MiscItem.count') do
      post :create, misc_item: { name: @misc_item.name, price: @misc_item.price }
    end

    assert_redirected_to misc_item_path(assigns(:misc_item))
  end

  test "should show misc_item" do
    get :show, id: @misc_item
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @misc_item
    assert_response :success
  end

  test "should update misc_item" do
    patch :update, id: @misc_item, misc_item: { name: @misc_item.name, price: @misc_item.price }
    assert_redirected_to misc_item_path(assigns(:misc_item))
  end

  test "should destroy misc_item" do
    assert_difference('MiscItem.count', -1) do
      delete :destroy, id: @misc_item
    end

    assert_redirected_to misc_items_path
  end
end
