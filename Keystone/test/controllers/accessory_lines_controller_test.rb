require 'test_helper'

class AccessoryLinesControllerTest < ActionController::TestCase
  setup do
    @accessory_line = accessory_lines(:one)
  end

  test "should get index" do
    get :index
    assert_response :success
    assert_not_nil assigns(:accessory_lines)
  end

  test "should get new" do
    get :new
    assert_response :success
  end

  test "should create accessory_line" do
    assert_difference('AccessoryLine.count') do
      post :create, accessory_line: { accessory_id: @accessory_line.accessory_id, bidSheet_id: @accessory_line.bidSheet_id }
    end

    assert_redirected_to accessory_line_path(assigns(:accessory_line))
  end

  test "should show accessory_line" do
    get :show, id: @accessory_line
    assert_response :success
  end

  test "should get edit" do
    get :edit, id: @accessory_line
    assert_response :success
  end

  test "should update accessory_line" do
    patch :update, id: @accessory_line, accessory_line: { accessory_id: @accessory_line.accessory_id, bidSheet_id: @accessory_line.bidSheet_id }
    assert_redirected_to accessory_line_path(assigns(:accessory_line))
  end

  test "should destroy accessory_line" do
    assert_difference('AccessoryLine.count', -1) do
      delete :destroy, id: @accessory_line
    end

    assert_redirected_to accessory_lines_path
  end
end
