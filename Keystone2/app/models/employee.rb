class Employee < ActiveRecord::Base
  has_many :bid_sheets

  validates :firstName, :lastName, presence: true
  validates :firstName, :lastName, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters." }

  def set_empfullname
    empfullname = firstName + ' ' + lastName
    return empfullname
  end
end
