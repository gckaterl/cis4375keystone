class AccessoryLine < ActiveRecord::Base
  belongs_to :accessory
  belongs_to :bid_sheet, :foreign_key => :bidSheet_id
end