class Accessory < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  has_many :bid_sheets

#  validates :modelNumber, :productDescription, presence: true
#  validates :dealer, :msrp, numericality: true

  def desc_with_price
    "#{productDescription} - #{number_to_currency(dealer)}"
  end

end
