class ConcreteSlab < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  has_many :bid_sheets

  validates :slabName, :slabPrice, presence: true
  validates :slabPrice, numericality: true

  def name_with_price
    "#{slabName} - #{number_to_currency(slabPrice)}"
  end
end
