class TransferSwitch < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  has_many :bid_sheets

  def model_with_price
    "#{modelNumber} - #{number_to_currency(dealer)}"
  end

end
