class Customer < ActiveRecord::Base
  belongs_to :state
  has_many :bid_sheets

#  validates :firstName, :lastName, :streetAddress, :city, :state_id, :zipCode, :phone, presence: true
#  validates :firstName, :lastName, :city, format: { with: /\A[a-zA-Z]+\z/, message: "only allows letters." }
#  validates :phone2, length: {is:10}, numericality: {only_integer: true}, allow_blank: true
#  validates :phone, length: {is:10}, numericality: {only_integer: true}

  def set_cusfullname
    cusfullname = firstName + ' ' + lastName
    return cusfullname
  end

  def set_cusaddress
    cusaddress = streetAddress + ' ' + streetAddress2 + ', ' + city + ', ' + state.abbreviation + '  ' + zipCode.to_s
    return cusaddress
  end
end
