class Generator < ActiveRecord::Base
  include ActionView::Helpers::NumberHelper

  has_many :bid_sheets

  validates :modelNumber, :productDescription, presence: true
  validates :dealer, :msrp, :map, numericality: true

  def model_with_price
    "#{modelNumber} - #{number_to_currency(dealer)}"
  end

end
