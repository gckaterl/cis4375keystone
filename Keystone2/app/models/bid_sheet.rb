class BidSheet < ActiveRecord::Base
  belongs_to :employee
  belongs_to :customer
  belongs_to :transfer_switch, :foreign_key => :transferswitch_id
  belongs_to :concrete_slab, :foreign_key => :concreteslab_id
  belongs_to :generator
  belongs_to :accessory

  validates :jobNumber, uniqueness: { scope: :jobNumber, message: "%Job number: {value} has already been used." }

end
