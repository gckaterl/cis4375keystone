class AddFieldsToBidSheet < ActiveRecord::Migration
  def change
    change_table :bid_sheets do |t|

    t.decimal :gasLineInstallation
    t.decimal :gasFootage
    t.integer :gasLength
    t.decimal :gasRegulator
    t.decimal :gasMeterUpgrade
    t.decimal :electricalSupply
    t.decimal :craneRigging
    t.decimal :deliveryShipping
    t.decimal :permits
    t.decimal :batteries
    t.string :miscItemName1
    t.decimal :miscItemPrice1
    t.string :miscItemName2
    t.decimal :miscItemPrice2
    t.string :miscItemName3
    t.decimal :miscItemPrice3
    t.integer :accessory1
    t.integer :accessory2
    t.integer :accessory3
    t.integer :accessory4
    t.integer :accessory5
    t.integer :accessory6
    t.integer :accessory7
    t.integer :accessory8
    t.integer :accessory9
    t.integer :accessory10
    end
  end
end
